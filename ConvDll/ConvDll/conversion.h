#pragma once

#ifdef CONVDLL_EXPORTS
#define CONVDLL_API __declspec(dllexport)
#else
#define CONVDLL_API __declspec(dllimport)
#endif

extern "C" CONVDLL_API int asciiBinaryToInt(char *s);
extern "C" CONVDLL_API int asciiHEXToInt(char *s);
extern "C" CONVDLL_API double asciiToDouble(char *s);

