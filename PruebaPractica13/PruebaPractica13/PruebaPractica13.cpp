// PruebaPractica13.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "conversion.h"

int main()
{
	int iOpcion;
	char cArrResp[50];

	//MENU para llamar funciones
	do {
		printf("CONVERSIONES:\n");
		printf("1. Binario a Entero:\n");
		printf("2. Hexadecimal a Entero:\n");
		printf("3. ASCII a Decimal\n");
		printf("0. Salir\n");
		printf("Seleccionar Opcion: ");

		scanf_s("%d", &iOpcion);

		switch (iOpcion)
		{
		case 1:
			printf("Ingresa numero binario: ");
			scanf_s("%s", cArrResp, (unsigned)_countof(cArrResp));
			printf("Numero entero: %d\n", asciiBinaryToInt(cArrResp));
			break;
		case 2:
			printf("Ingresa numero Hexadecimal: ");
			scanf_s("%s", cArrResp, (unsigned)_countof(cArrResp));
			printf("Numero entero: %d\n", asciiHEXToInt(cArrResp));
			break;
		case 3:
			printf("Ingresa secuencia de caracteres numericos: ");
			scanf_s("%s", cArrResp, (unsigned)_countof(cArrResp));
			printf("Numero decimal: %lf\n", asciiToDouble(cArrResp));
			break;
		case 0:
			printf("Gracias!\n");
			break;
		default:
			printf("Opcion invalida\n");
		}

	} while (iOpcion != 0);

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
